#include "prodcon_config.h"
/***************************************************************************************/

#include <ArduinoJson.h>

/***************************************************************************************/

#include <Wire.h>
#include "uEEPROMLib.h"
uEEPROMLib eeprom(0x50);
unsigned int pos;

/***************************************************************************************/
#include "DMD3.h"
#include "Droid_Sans_16.h"
#include "DejaVuSansBold9.h"
#include "angka6x13.h"

DMD3 display (1, 3);
char dmdBuff1[10];
unsigned int cerah = 200;

// Generally, you should use "unsigned long" for variables that hold time
// The value will quickly become too large for an int to store
unsigned long previousMillis = 0;        // will store last time code was updated

// constants won't change:
const long interval = 1000;           // interval at which to execute the code (milliseconds)


unsigned int iVal[MAX_ARRAY] = {0, 0, 0, 0, 0, 0, 0};
int remote_menu = 0;
int remote_cmd = 0;
unsigned int iVal_temp[MAX_ARRAY] = {0, 0, 0, 0, 0, 0, 0};
//int remote_menu_temp = 0;
//int remote_cmd_temp = 0;

int select_tt = 0;
unsigned int TT[5];
int TT_select;
int temp_TT_select;
int lock = 1;
int batas_per_tt = 3;
unsigned int temp_tt = 0;
int tt_oke = 0;
int lock_diff = 0;
int ok = 0;
int prestate = 0 ;


/***************************************************************************************/
void scan()
{
  display.refresh();
}

void board_counting() {

  //menjalankan proses counting actual berdasarkan tt dan button
  //tt yang dipilih sesuai jumper pada terminal block, read pinnya
  //timer untuk tt bisa pakai millis() atau rtc interrupt
  //tampilan counterboard:
  //kolom atas nilai target
  //kolom tengah nilai actual
  //kolom bawah nilai difference

  if (millis() - previousMillis >= interval) {
    previousMillis = millis();

    //eeprom
    for (int i = 0; i <= 6; i++) {
      if (iVal_temp[i] != iVal[i]) {
        iVal_temp[i] = iVal[i];
        memWriteInt(i * 10, iVal[i]);

      }
    }

#ifdef debug_serial
    Serial.println(temp_tt);
    Serial.print("TT ");
    Serial.print( temp_TT_select);
    Serial.print(": ");
    Serial.println(iVal[TT_select]);
    Serial.print("TARGET: ");
    Serial.println(iVal[0]);
    Serial.print("ACTUAL: ");
    Serial.println(iVal[1]);
    Serial.print("DIFFERENCE: ");
    Serial.println(iVal[2]);
    Serial.print("lock : ");
    Serial.println(lock);
    Serial.println("************************************************");
    Serial.println();
#endif

    Tampil_angka_A(iVal[0]);
    Tampil_angka_T(iVal[1]);
    Tampil_angka_B_min(iVal[2]);

    temp_tt = temp_tt + 1;

    if (iVal[0] == iVal[1])
    {
      remote_cmd = HolD;
    }
    else
    {
      if (temp_tt > iVal[TT_select]) {
        temp_tt = 1;
        if (lock > 1)
        {
          lock = 1;
        }
        else
        {
          iVal[2]++;
        }

      }
    }
  }


  if (button_counter == LOW && prestate == 0 ) {

    lock = lock + 1;
    prestate = 1;
    //      Serial.println("reset TT");
    // diff tidak akan berkurang kalo belum pencet 2 kali di dalam satu TT
    if (lock > 2 )
    {
      if (iVal[2] == 0)
      {
        iVal[2] = 0;
      }
      else
      {
        iVal[2] = iVal[2] - 1;
      }


      //actual bertambah
      iVal[1]++;
      Tampil_angka_A(iVal[0]);
      Tampil_angka_T(iVal[1]);
      Tampil_angka_B_min(iVal[2]);
    }
    else
    {
      iVal[1]++;
      Tampil_angka_A(iVal[0]);
      Tampil_angka_T(iVal[1]);
      Tampil_angka_B_min(iVal[2]);
    }



    delay(delay_button);

  } else if (button_counter == HIGH)
  { delay(delay_button);
    prestate = 0;
  }
}



void board_hold() {
  //proses counting actual berhenti
  //tampilan counterboard:
  //kolom atas ----
  //kolom tengah HOLD
  //kolom bawah ----
  temp_tt = 0;
  Tampil_Hold();

  if (millis() - previousMillis >= interval) {
    previousMillis = millis();
#ifdef debug_serial
    Serial.println("----");
    Serial.println("HOLD");
    Serial.println("----");
    Serial.println();
    Serial.println("************************************************");
    Serial.println();
#endif

  }
}

void board_changeval(uint8_t select_menu) {
  //proses counting actual berhenti
  //menampilkan perubahan nilai sesuai kiriman dari remote
  //tampilan counterboard:
  //kolom atas variabel yg diubah
  //kolom tengah nilai yg diubah
  //kolom bawah nilai yg diubah

  switch (select_menu) {
    case MENU_TARGET:

      if (millis() - previousMillis >= interval) {
        previousMillis = millis();
#ifdef debug_serial
        Serial.println("ETGT");
        Serial.println(iVal[0]);
        Serial.println(iVal[0]);
#endif
        Tampil_text_A("ETGT");
        Tampil_angka_T(iVal[0]);
        Tampil_angka_B(iVal[0]);
      }
      break;
    case MENU_DIFF:

      if (millis() - previousMillis >= interval) {
        previousMillis = millis();
#ifdef debug_serial
        Serial.println("CTGT");
        Serial.println(iVal[1]);
        Serial.println(iVal[1]);
#endif
        Tampil_text_A("CTGT");
        Tampil_angka_T(iVal[1]);
        Tampil_angka_B(iVal[1]);
      }
      break;
    case MENU_ACTUAL:

      if (millis() - previousMillis >= interval) {
        previousMillis = millis();
#ifdef debug_serial
        Serial.println("ACTL");
        Serial.println(iVal[2]);
        Serial.println(iVal[2]);
#endif
        Tampil_text_A("ACTL");
        Tampil_angka_T(iVal[2]);
        Tampil_angka_B(iVal[2]);
      }
      break;
    case MENU_TT1:

      if (millis() - previousMillis >= interval) {
        previousMillis = millis();
#ifdef debug_serial
        Serial.println("TT 1");
        Serial.println(iVal[3]);
        Serial.println(iVal[3]);
#endif
        Tampil_text_A("TT 1");
        Tampil_angka_T(iVal[3]);
        Tampil_angka_B(iVal[3]);
      }
      break;
    case MENU_TT2:

      if (millis() - previousMillis >= interval) {
        previousMillis = millis();
#ifdef debug_serial
        Serial.println("TT 2");
        Serial.println(iVal[4]);
        Serial.println(iVal[4]);
#endif
        Tampil_text_A("TT 2");
        Tampil_angka_T(iVal[4]);
        Tampil_angka_B(iVal[4]);
      }
      break;
    case MENU_TT3:

      if (millis() - previousMillis >= interval) {
        previousMillis = millis();
#ifdef debug_serial
        Serial.println("TT 3");
        Serial.println(iVal[5]);
        Serial.println(iVal[5]);
#endif
        Tampil_text_A("TT 3");
        Tampil_angka_T(iVal[5]);
        Tampil_angka_B(iVal[5]);
      }
      break;
    case MENU_TT4:

      if (millis() - previousMillis >= interval) {
        previousMillis = millis();
#ifdef debug_serial
        Serial.println("TT 4");
        Serial.println(iVal[6]);
        Serial.println(iVal[6]);
#endif
        Tampil_text_A("TT 4");
        Tampil_angka_T(iVal[6]);
        Tampil_angka_B(iVal[6]);
      }
      break;
    default:
      break;
  }
#ifdef debug_serial
  Serial.println();
  Serial.println("************************************************");
  Serial.println();
#endif
}

uint8_t memWriteInt(uint8_t addr, uint32_t int_value)
{
  unsigned int ret = 0;
  if (!eeprom.eeprom_write(addr, int_value)) {
    ret = 1;
#ifdef debug_serial
    Serial.println("Failed to store INT");
#endif
  } else {
    ret = 0;
#ifdef debug_serial
    Serial.println("INT correctly stored");
#endif
  }
  return ret;
}

uint32_t memReadInt(uint8_t addr)
{
  unsigned int val_temp = 0;
  eeprom.eeprom_read(addr, &val_temp);
#ifdef debug_serial
  Serial.print("addr ");
  Serial.print(addr);
  Serial.print(" : ");
  Serial.println(val_temp);
#endif
  return val_temp;
}
/***************************************************************************************/

void setup() {
  Serial.begin(9600);
  remote_cmd = HolD_1;
  remote_menu = MENU_HOME;
  pinMode(pin_TT1, INPUT_PULLUP);
  pinMode(pin_TT2, INPUT_PULLUP);
  pinMode(pin_TT3, INPUT_PULLUP);
  pinMode(pin_TT4, INPUT_PULLUP);
  pinMode(pin_counter, INPUT_PULLUP);
  pinMode(pin_minus, OUTPUT);




  //read eeprom
  Wire.begin();

  //  memWriteInt(0, 100);
  //  memWriteInt(10, 100);
  //  memWriteInt(20, 100);
  //  memWriteInt(30, 100);
  //  memWriteInt(40, 100);
  //  memWriteInt(50, 100);
  //  memWriteInt(60, 100);

  iVal[0] = memReadInt(0);
  iVal[1] = memReadInt(10);
  iVal[2] = memReadInt(20);
  iVal[3] = memReadInt(30);
  iVal[4] = memReadInt(40);
  iVal[5] = memReadInt(50);
  iVal[6] = memReadInt(60);

  iVal_temp[0] = iVal[0];
  iVal_temp[1] = iVal[1];
  iVal_temp[2] = iVal[2];
  iVal_temp[3] = iVal[3];
  iVal_temp[4] = iVal[4];
  iVal_temp[5] = iVal[5];
  iVal_temp[6] = iVal[6];

  delay(500);
  for (int i = 0; i <= 6; i++) {
#ifdef debug_serial
    Serial.print("iVal");
    Serial.print(i);
    Serial.print(" :");
    Serial.println(iVal[i]);
#endif
  }
#ifdef debug_serial
  Serial.println("Nungguin TT");
#endif
  Timer1.initialize(2000);
  Timer1.attachInterrupt(scan);
  Timer1.pwm(AO, cerah);
  Tampil_Hold();
  while (1)
  {

    if (!button_TT1) {
#ifdef debug_serial
      Serial.println("TT1");
#endif
      TT_Messg("1");
      TT_select = 3;
      temp_TT_select = 1;
      delay(2000);
      break;
    } else if (!button_TT2) {
#ifdef debug_serial
      Serial.println("TT2");
#endif
      TT_Messg("2");
      TT_select = 4;
      temp_TT_select = 2;
      delay(2000);
      break;
    } else if (!button_TT3) {
#ifdef debug_serial
      Serial.println("TT3");
#endif
      TT_Messg("3");
      TT_select = 5;
      temp_TT_select = 3;
      delay(2000);
      break;
    } else if (!button_TT4) {
#ifdef debug_serial
      Serial.println("TT4");
#endif
      TT_Messg("4");
      TT_select = 6;
      temp_TT_select = 4;
      delay(2000);
      break;
    }
  }

#ifdef debug_serial
  Serial.println("TToke");
#endif
  display.clear();
}

/***************************************************************************************/

void loop() {


  if (remote_cmd == RuN || remote_cmd == ContinuE) {
    board_counting();

  }
  if (remote_cmd == ReseT) {
    iVal[1] = 0 ;
    iVal[2] = 0 ;
    temp_tt = 0;
    board_counting();
  }
  if (remote_cmd == HolD_1) {
    Tampil_Hold();
  }
  else if (remote_cmd == HolD ) {

    Tampil_Hold_angka();
    temp_tt = 0;
  }
  else if (remote_cmd == SenD && remote_menu >= MENU_TARGET && remote_menu <= MENU_TT4) {
    board_changeval(remote_menu);
    temp_tt = 0;
  }
  else if (remote_cmd == SenD && remote_menu == MENU_HOME) {
    board_hold();
    temp_tt = 0;
  }


  // Check if the other Arduino is transmitting
  if (Serial.available())
  {

    // Allocate the JSON document
    // This one must be bigger than for the sender because it must store the strings
    StaticJsonDocument<200> doc;

    // Read the JSON document from the "link" serial port
    DeserializationError err = deserializeJson(doc, Serial);


    if (err == DeserializationError::Ok)
    {
      int buff_doc = doc["cmd"];
      int buff_m   = doc["menu"];
      if (buff_doc == RuN || buff_doc == ContinuE || buff_doc == HolD )
      {
        remote_cmd = doc["cmd"];
      }
      else if (buff_doc == ReseT)
      {
        remote_cmd = doc["cmd"];
      }
      else if (buff_doc == SenD )
      {
        if (buff_m == MENU_TARGET)  {
          iVal[0] = doc["etgt"];
          remote_cmd = doc["cmd"];
           remote_menu = doc["menu"];
        }
        else if (buff_m == MENU_DIFF)    {
          iVal[1] = doc["ctgt"];
          remote_cmd = doc["cmd"];
           remote_menu = doc["menu"];
        }
        else if (buff_m == MENU_ACTUAL)  {
          iVal[2] = doc["actl"];
          remote_cmd = doc["cmd"];
           remote_menu = doc["menu"];
        }
        else if (buff_m == MENU_TT1)     {
          iVal[3] = doc["tt1"];
          remote_cmd  = doc["cmd"];
           remote_menu = doc["menu"];
        }
        else if (buff_m == MENU_TT2)     {
          iVal[4] = doc["tt2"];
          remote_cmd  = doc["cmd"];
           remote_menu = doc["menu"];
        }
        else if (buff_m == MENU_TT3)     {
          iVal[5] = doc["tt3"];
          remote_cmd  = doc["cmd"];
           remote_menu = doc["menu"];
        }
        else if (buff_m == MENU_TT4)     {
          iVal[6] = doc["tt4"];
          remote_cmd  = doc["cmd"];
           remote_menu = doc["menu"];
        }
        else
        {
          remote_cmd = doc["cmd"];
          remote_menu = doc["menu"];
        }
      }
      else
      {
        remote_cmd = doc["cmd"];
        remote_menu = doc["menu"];
      }
      for (int i = 0; i <= 6; i++) {
        if (iVal_temp[i] != iVal[i]) {
          iVal_temp[i] = iVal[i];
          memWriteInt(i * 10, iVal[i]);
          delay(500);
        }
      }
      display.clear();
      char buf[16];
#ifdef debug_serial
      sprintf(buf, "etgt: %d", iVal[0]);
      Serial.println(buf);
      sprintf(buf, "ctgt: %d", iVal[1]);
      Serial.println(buf);
      sprintf(buf, "actl: %d", iVal[2]);
      Serial.println(buf);
      sprintf(buf, "tt1: %d", iVal[3]);
      Serial.println(buf);
      sprintf(buf, "tt2: %d", iVal[4]);
      Serial.println(buf);
      sprintf(buf, "tt3: %d", iVal[5]);
      Serial.println(buf);
      sprintf(buf, "tt4: %d", iVal[6]);
      Serial.println(buf);
      sprintf(buf, "cmd: %d", remote_cmd);
      Serial.println(buf);
      sprintf(buf, "menu: %d", remote_menu);
      Serial.println(buf);
      Serial.println();
      Serial.println("************************************************");
      Serial.println();
#endif

    }
    else
    {
      // Print error to the "debug" serial port
#ifdef debug_serial
      Serial.print("deserializeJson() returned ");
      Serial.println(err.c_str());
#endif

      // Flush all bytes in the "link" serial port buffer
      while (Serial.available() > 0)
        Serial.read();
    }

  }
}


//===================================================================================================================================
void Tampil_Hold()
{
  //
  display.setFont(DejaVuSansBold9);
  display.drawText(3, 20, "HOLD");

  display.drawText(5, 0, "____");
  display.drawText(5, 32, "____");
}

void Tampil_Hold_angka()
{

  if (ok == 0) {
    display.clear();
    display.setFont(DejaVuSansBold9);
    display.drawText(3, 20, "HOLD");

    display.drawText(5, 0, "____");
    display.drawText(5, 32, "____");
    delay (2000);
    display.clear();
    ok = 1;
  }
  Tampil_angka_A(iVal[0]);
  Tampil_angka_T(iVal[1]);
  Tampil_angka_B_min(iVal[2]);

}
void Tampil_angka_T(int angka)
{
  sprintf(dmdBuff1, "%.4d", angka);
  display.setFont(angka6x13);
  display.drawText(3, 17, dmdBuff1);

}
void Tampil_angka_A(int angka)
{
  sprintf(dmdBuff1, "%.4d", angka);
  display.setFont(angka6x13);
  display.drawText(3, 1, dmdBuff1);

}
void Tampil_angka_B(int angka)
{
  sprintf(dmdBuff1, "%.4d", angka);
  display.setFont(angka6x13);
  display.drawText(3, 33, dmdBuff1);

}
void Tampil_angka_B_min(int angka)
{
  display.setFont(DejaVuSansBold9);
  display.drawLine(1, 41,5,41);
  display.drawLine(1, 40,5,40);
  sprintf(dmdBuff1, "%.3d", angka);
  display.setFont(angka6x13);
  display.drawText(10, 33, dmdBuff1);
}

void Tampil_text_A(char text[10])
{
  display.setFont(DejaVuSansBold9);
  display.drawText(3, 3, text);
}
void Tampil_text_T(char text[10])
{
  display.setFont(DejaVuSansBold9);
  display.drawText(3, 3, text);
}
void Tampil_text_B(char text[10])
{
  display.setFont(DejaVuSansBold9);
  display.drawText(3, 3, text);
}
void TT_Messg(char tt[2])
{
  display.clear();
  display.setFont(DejaVuSansBold9);
  display.drawText(7, 3, "T T");
  display.setFont(angka6x13);
  display.drawText(13, 17, tt);
}
