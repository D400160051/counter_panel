#define  MENU_PASSWORD 0
#define  MENU_HOME 1
#define  MENU_TARGET 2
#define  MENU_DIFF 3
#define  MENU_ACTUAL 4
#define  MENU_TT1 5
#define  MENU_TT2 6
#define  MENU_TT3 7
#define  MENU_TT4 8
#define  MENU_SEND 9
#define  MENU_RUN 10
#define  MENU_CLEAR 11
#define  MENU_HOLD 12
#define  MENU_PSW 13
#define  MAX_MENU 14

#define MAX_ARRAY 7

#define SenD 0
#define RuN 1
#define HolD 2
#define ContinuE 3
#define ReseT 4
#define HolD_1 5

#define pin_TT1 A0
#define pin_TT2 A1 
#define pin_TT3 A2
#define pin_TT4 A3
#define pin_counter 2
#define pin_minus 3
#define AO 9
#define delay_button 100
//#define debug_serial

#define button_TT1 digitalRead(pin_TT1)
#define button_TT2 digitalRead(pin_TT2)
#define button_TT3 digitalRead(pin_TT3)
#define button_TT4 digitalRead(pin_TT4)

#define button_counter digitalRead(pin_counter)


#define button_notpressed  button_TT1 && button_TT2 && button_TT3 && button_TT4 && button_counter1
#define button_pressed !(button_notpressed)
