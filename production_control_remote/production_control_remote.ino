#include "prodcon_config.h"


/***************************************************************************************/

#include <ArduinoJson.h>
#include <SoftwareSerial.h>
// Declare the "link" serial port
// Please see SoftwareSerial library for detail
SoftwareSerial linkSerial(10, 11); // RX, TX

/***************************************************************************************/

#include <LiquidCrystal_I2C.h>
LiquidCrystal_I2C lcd(0x27, 16, 2);

/***************************************************************************************/

#include <Password.h>
String newPasswordString; //hold the new password
char newPassword[4]; //charater string of newPasswordString
//initialize password to 1234
//you can use password.set(newPassword) to overwrite it
Password password = Password("5476");
byte maxPasswordLength = 4;
byte currentPasswordLength = 0;

/***************************************************************************************/

#include <Keypad.h>

const byte ROWS = 4; //four rows
const byte COLS = 4; //four columns

char keys[ROWS][COLS] = {
  {'1', '2', '3', 'A'},
  {'4', '5', '6', 'B'},
  {'7', '8', '9', 'C'},
  {'*', '0', '#', 'D'}
};

byte rowPins[ROWS] = {2, 3, 4, 5}; //connect to the row pinouts of the keypad
byte colPins[COLS] = {6, 7, 8, 9}; //connect to the column pinouts of the keypad

//Create an object of keypad
Keypad kpd = Keypad( makeKeymap(keys), rowPins, colPins, ROWS, COLS );

int kstateStarTemp = 0;
int kstateBTemp = 0;
int statehold = 0;
/***************************************************************************************/

int remote_menu;
unsigned long menuTime;

unsigned long loopCount;
unsigned long keypadTime;
String msg;

#define MAX_NUM 5
String inputString = "";
uint16_t inputInt = 0;

uint8_t menu_temp = 0;
//uint8_t flagFirstBoot = 0;
//uint8_t flagResetCounter = 0;
uint8_t isUnlocked = 0;
#define MAX_ARRAY 7
int iVal[MAX_ARRAY] = {0, 0, 0, 0, 0, 0, 0};

/***************************************************************************************/

void processNumberKey(char key) {
  Serial.println(key);
  currentPasswordLength++;
  uint8_t xPos = 5;
  lcd.setCursor(currentPasswordLength + xPos, 1);  //This to write "*" on the LCD whenever a key is pressed it's position is controlled by j
  lcd.print("*");
  password.append(key);
  if (currentPasswordLength == maxPasswordLength) {
    checkPassword();
  }
}

void checkPassword() {
  delay(1000);
  if (password.evaluate()) {
    isUnlocked = 1;
    remote_menu = MENU_HOME;
    Serial.println(" OK.");
    lcd.clear();
    lcd.setCursor(1, 0);
    lcd.print("PASSWORD BENAR");          //Message to print when the code is wrong
    lcd.setCursor(1, 1);
    lcd.print("LANJUT KE MENU");
    delay(2000);
    lcd.clear();
  } else {
    delay(1000);
    Serial.println(" Wrong passwowrd!");
    lcd.clear();
    lcd.setCursor(1, 0);
    lcd.print("PASSWORD SALAH");          //Message to print when the code is wrong
    lcd.setCursor(3, 1);
    lcd.print("COBA LAGI!");
    delay(2000);
    lcd.clear();
    resetPassword();
  }
  resetPassword();
}

void resetPassword() {
  lcd.clear();
  password.reset();
  currentPasswordLength = 0;
}

/***************************************************************************************/

void menu_password() {
  lcd.setCursor(1, 0);
  lcd.print("ENTER PASSWORD");
  char key = kpd.getKey();
  if (key != NO_KEY) {
    delay(50);
    switch (key) {
      case 'A':
        break;
      case 'B':
        resetPassword();
        break;
      case 'C':
        break;
      case 'D':
        break;
      case '#':
        break;
      case '*':
        break;
      default:
        processNumberKey(key);
    }
  }
}

void showLastVal() {
  lcd.setCursor(6, 1);
  lcd.print(iVal[remote_menu - MENU_TARGET]);
}

void menu_home() {
  if (statehold == 0) {
    lcd.setCursor(0, 0);
    lcd.print("HILEX  INDONESIA");
    lcd.setCursor(0, 1);
    lcd.print("DISPLAY  CONTROL");
  } else {
    lcd.setCursor(0, 0);
    lcd.print("----- HOLD -----");
    lcd.setCursor(0, 1);
    lcd.print("                ");
  }
}

void menu_target() {
  lcd.setCursor(3, 0);
  lcd.print("UBAH NILAI");
  lcd.setCursor(0, 1);
  lcd.print("ETGT:");
  //  lcd.setCursor(7, 1);
  //  lcd.print(inputString);
}

void menu_diff() {
  lcd.setCursor(3, 0);
  lcd.print("UBAH NILAI");
  lcd.setCursor(0, 1);
  lcd.print("CTGT:");
  //  lcd.setCursor(7, 1);
  //  lcd.print(inputString);
}

void menu_actual() {
  lcd.setCursor(3, 0);
  lcd.print("UBAH NILAI");
  lcd.setCursor(0, 1);
  lcd.print("ACTL:");
  //  lcd.setCursor(7, 1);
  //  lcd.print(inputString);
}

void menu_ttone() {
  lcd.setCursor(3, 0);
  lcd.print("UBAH NILAI");
  lcd.setCursor(0, 1);
  lcd.print("Tt'1:");
  //  lcd.setCursor(7, 1);
  //  lcd.print(inputString);
}

void menu_tttwo() {
  lcd.setCursor(3, 0);
  lcd.print("UBAH NILAI");
  lcd.setCursor(0, 1);
  lcd.print("Tt'2:");
  //  lcd.setCursor(7, 1);
  //  lcd.print(inputString);
}

void menu_ttthree() {
  lcd.setCursor(3, 0);
  lcd.print("UBAH NILAI");
  lcd.setCursor(0, 1);
  lcd.print("Tt'3:");
  //  lcd.setCursor(7, 1);
  //  lcd.print(inputString);
}

void menu_ttfour() {
  lcd.setCursor(3, 0);
  lcd.print("UBAH NILAI");
  lcd.setCursor(0, 1);
  lcd.print("Tt'4:");
  //  lcd.setCursor(7, 1);
  //  lcd.print(inputString);
}

void menu_loop() {
  if ((millis() - menuTime) > 1000 || menu_temp != remote_menu) {
    Serial.print("remote_menu = ");
    Serial.println(remote_menu);
    menuTime = millis();
    loopCount = 0;
  }
  if (menu_temp != remote_menu) {
    lcd.clear();
    if (remote_menu >= MENU_TARGET && remote_menu <= MENU_TT4)
      showLastVal();
  }
  switch (remote_menu) {
    case MENU_PASSWORD:
      menu_password();
      break;
    case MENU_HOME:
      menu_home();
      break;
    case MENU_TARGET:
      menu_target();
      break;
    case MENU_DIFF:
      menu_diff();
      break;
    case MENU_ACTUAL:
      menu_actual();
      break;
    case MENU_TT1:
      menu_ttone();
      break;
    case MENU_TT2:
      menu_tttwo();
      break;
    case MENU_TT3:
      menu_ttthree();
      break;
    case MENU_TT4:
      menu_ttfour();
      break;
    default:
      break;
  }
  menu_temp = remote_menu;
}

/***************************************************************************************/

void keypad_loop() {
  loopCount++;
  if ( (millis() - keypadTime) > 5000 ) {
    Serial.print("Average keypad_loop() per second = ");
    Serial.println(loopCount / 5);
    keypadTime = millis();
    loopCount = 0;
    //    kstateStarTemp = IDLE;
  }

  //   Fills kpd.key[ ] array with up-to 10 active keys.
  //   Returns true if there are ANY active keys.

  if (kpd.getKeys())
  {
    for (int i = 0; i < LIST_MAX; i++) // Scan the whole key list.
    {
      if (kpd.key[i].stateChanged)   // Only find keys that have changed state.
      {

        switch (kpd.key[i].kstate) {  // Report active key state : IDLE, PRESSED, HOLD, or RELEASED
          case PRESSED:
            msg = " PRESSED.";
            break;
          case HOLD:
            msg = " HOLD.";
            break;
          case RELEASED:
            msg = " RELEASED.";
            break;
          case IDLE:
            msg = " IDLE.";
        }
        Serial.print("Key ");
        Serial.print(kpd.key[i].kchar);
        Serial.println(msg);

        if (kpd.key[i].kchar == 'B' && kpd.key[i].kstate == HOLD) {
          statehold = 0;
          send_json(ReseT);
          lcd.clear();
          lcd.setCursor(0, 0);
          lcd.print("----- RESET ----");
          delay(2000);
          lcd.clear();
          if (remote_menu >= MENU_TARGET && remote_menu <= MENU_TT4)
            showLastVal();
        }

        if (kpd.key[i].kchar == 'C' && kpd.key[i].kstate == HOLD) {
          statehold = 0;
          send_json(SenD);
          lcd.clear();
          lcd.setCursor(0, 0);
          lcd.print("----- SEND -----");
          delay(2000);
          lcd.clear();
          if (remote_menu >= MENU_TARGET && remote_menu <= MENU_TT4)
            showLastVal();
        }

        if (kpd.key[i].kchar == 'D' && kpd.key[i].kstate == HOLD) {
          statehold = 0;
          send_json(RuN);
          lcd.clear();
          lcd.setCursor(0, 0);
          lcd.print("----- RUN ------");
          delay(2000);
          lcd.clear();
          if (remote_menu >= MENU_TARGET && remote_menu <= MENU_TT4)
            showLastVal();
        }

        if (kpd.key[i].kchar == '*' && (kpd.key[i].kstate == RELEASED)) {
          if (kstateStarTemp != HOLD) {
            statehold = 0;
            send_json(ContinuE);
            lcd.clear();
            lcd.setCursor(0, 0);
            lcd.print("--- CONTINUE ---");
            delay(2000);
            lcd.clear();
            if (remote_menu >= MENU_TARGET && remote_menu <= MENU_TT4)
              showLastVal();
          }
          kstateStarTemp = IDLE;
        }

        if (kpd.key[i].kchar == '*' && kpd.key[i].kstate == HOLD) {
          statehold = 1;
          send_json(HolD);
          kstateStarTemp = HOLD;
          lcd.clear();
          lcd.setCursor(0, 0);
          lcd.print("----- HOLD -----");
          delay(2000);
          //          lcd.clear();
        }

        if (kpd.key[i].kchar == '#' && kpd.key[i].kstate == HOLD) {
          statehold = 0;
          isUnlocked = 0;
          remote_menu = MENU_PASSWORD;
          delay(500);
        }

        if (remote_menu == MENU_HOME) {
          if (kpd.key[i].kchar == 'A' && kpd.key[i].kstate == PRESSED) {
            statehold = 0;
            remote_menu++;
          }
        }

        else if (remote_menu >= MENU_TARGET && remote_menu <= MENU_TT4) {

          if (kpd.key[i].kchar >= '0' && kpd.key[i].kchar <= '9' && kpd.key[i].kstate == PRESSED) {     // only act on numeric keys
            lcd.clear();
            if (inputString.length() < 4) {
              inputString += kpd.key[i].kchar;               // append new character to input string
            }
            if (inputString.length() > 0) {
              inputInt = inputString.toInt(); // YOU GOT AN INTEGER NUMBER
              Serial.println();
              Serial.print("INPUT NUMBER: ");
              Serial.println(inputInt);
              iVal[remote_menu - MENU_TARGET] = inputInt;
            }
            lcd.setCursor(6, 1);
            lcd.print(inputString);
          } else if (kpd.key[i].kchar == 'A' && kpd.key[i].kstate == PRESSED) {
            //            if (inputString.length() > 0) {
            //              inputInt = inputString.toInt(); // YOU GOT AN INTEGER NUMBER
            //              Serial.println();
            //              Serial.print("INPUT NUMBER: ");
            //              Serial.println(inputInt);
            //              iVal[remote_menu - MENU_TARGET] = inputInt;
            //            } else if (inputString.length() == 0) {
            //              inputInt = 0;
            //            }
            inputString = ""; // CLEAR INPUT
            inputInt = 0;

            char buf[32];
            for (int i = 0; i < MAX_ARRAY; i++) {
              sprintf(buf, "iVal[%d] = %d", i, iVal[i]);
              Serial.println(buf);
            }
            Serial.println();

            remote_menu++;
          } else if (kpd.key[i].kchar == 'B' && kpd.key[i].kstate == PRESSED) {
            inputString = "";                 // clear input
            lcd.clear();
          }
        }

      }
    }
  }

  if (remote_menu > MENU_TT4) {
    remote_menu = MENU_HOME;
  }
}

/***************************************************************************************/

//remote_cmd value -> 0 (no cmd); 1 (run); 2 (hold); 3 (continue); 4 (reset)
void send_json(uint8_t remote_cmd) {
  // Create the JSON document
  StaticJsonDocument<96> doc;
  doc["etgt"] = iVal[0];
  doc["ctgt"] = iVal[1];
  doc["actl"] = iVal[2];
  doc["tt1"] = iVal[3];
  doc["tt2"] = iVal[4];
  doc["tt3"] = iVal[5];
  doc["tt4"] = iVal[6];
  doc["cmd"] = remote_cmd;
  doc["menu"] = remote_menu;

  // Send the JSON document over the "link" serial port
  serializeJson(doc, linkSerial);
  serializeJson(doc, Serial);
  Serial.println();
}

void setup() {
  Serial.begin(9600);
  linkSerial.begin(9600);
  inputString.reserve(MAX_NUM); // maximum number of digit for a number is 10, change if needed
  loopCount = 0;
  keypadTime = millis();
  menuTime = millis();
  msg = "";
  lcd.init();                      // initialize the lcd
  lcd.backlight();
  remote_menu = MENU_HOME;
  menu_home();
  delay(2000);
  lcd.clear();
  iVal[3] = 10;
  iVal[4] = 10;
  iVal[5] = 10;
  iVal[6] = 10;
}

void loop() {
  if (isUnlocked == 0) {
    remote_menu = MENU_PASSWORD;
  } else {
    keypad_loop();
  }
  menu_loop();
}
