#include "prodcon_config.h"

/***************************************************************************************/

#include <ArduinoJson.h>
#include <SoftwareSerial.h>
// Declare the "link" serial port
// Please see SoftwareSerial library for detail
SoftwareSerial linkSerial(4, 5); // RX, TX

/***************************************************************************************/

#include <Wire.h>

#include "uEEPROMLib.h"
uEEPROMLib eeprom(0x57);
unsigned int pos;

/***************************************************************************************/

// Generally, you should use "unsigned long" for variables that hold time
// The value will quickly become too large for an int to store
unsigned long previousMillis = 0;        // will store last time code was updated

// constants won't change:
const long interval = 1000;           // interval at which to execute the code (milliseconds)

#define MAX_ARRAY 7
int iVal[MAX_ARRAY] = {0, 0, 0, 0, 0, 0, 0};
int remote_menu = 0;
int remote_cmd = 0;
int iVal_temp[MAX_ARRAY] = {0, 0, 0, 0, 0, 0, 0};
//int remote_menu_temp = 0;
//int remote_cmd_temp = 0;

int select_tt = 0;
unsigned int TT[5];
byte TT_select;
byte temp_TT_select;
int lock = 0;
unsigned int temp_tt = 0;

int tt1TerminalPin = A0;
int tt1TerminalVal = 0;
int tt2TerminalPin = A1;
int tt2TerminalVal = 0;
int tt3TerminalPin = A2;
int tt3TerminalVal = 0;
int tt4TerminalPin = A3;
int tt4TerminalVal = 0;

/***************************************************************************************/

void board_counting() {
  //menjalankan proses counting actual berdasarkan tt dan button
  //tt yang dipilih sesuai jumper pada terminal block, read pinnya
  //timer untuk tt bisa pakai millis() atau rtc interrupt
  //tampilan counterboard:
  //kolom atas nilai target
  //kolom tengah nilai actual
  //kolom bawah nilai difference
  if (millis() - previousMillis >= interval) {
    previousMillis = millis();
    Serial.println(temp_tt);
    Serial.print("TT ");
    Serial.print( temp_TT_select);
    Serial.print(": ");
    Serial.println(iVal[TT_select]);
    Serial.print("TARGET: ");
    Serial.println(iVal[0]);
    Serial.print("ACTUAL: ");
    Serial.println(iVal[1]);
    Serial.print("DIFFERENCE: ");
    Serial.println(iVal[2]);
    Serial.println();
    Serial.println("************************************************");
    Serial.println();
    temp_tt = temp_tt + 1;
    lock = lock - 1;
    if (temp_tt > iVal[TT_select]) {
      temp_tt = 1;
      Serial.println("diff -1");
      iVal[2]++;
      lock = 1;
    }

    

  }
  if (!button_counter && lock < 0 ) {
      lock = 1;
      Serial.println("reset TT");
      temp_tt = 0;
      if (iVal[2] > 0)
      {
        iVal[2] = iVal[2] - 1;
        iVal[1]++;
      }
      else
      {
        iVal[1]++;
      }
    }
}

void board_hold() {
  //proses counting actual berhenti
  //tampilan counterboard:
  //kolom atas ----
  //kolom tengah HOLD
  //kolom bawah ----
  if (millis() - previousMillis >= interval) {
    previousMillis = millis();
    Serial.println("----");
    Serial.println("HOLD");
    Serial.println("----");
    Serial.println();
    Serial.println("************************************************");
    Serial.println();
  }
}

void board_changeval(uint8_t select_menu) {
  //proses counting actual berhenti
  //menampilkan perubahan nilai sesuai kiriman dari remote
  //tampilan counterboard:
  //kolom atas variabel yg diubah
  //kolom tengah nilai yg diubah
  //kolom bawah nilai yg diubah
  switch (select_menu) {
    case MENU_TARGET:
      if (millis() - previousMillis >= interval) {
        previousMillis = millis();
        Serial.println("ETGT");
        Serial.println(iVal[0]);
        Serial.println(iVal[0]);
      }
      break;
    case MENU_DIFF:
      if (millis() - previousMillis >= interval) {
        previousMillis = millis();
        Serial.println("CTGT");
        Serial.println(iVal[1]);
        Serial.println(iVal[1]);
      }
      break;
    case MENU_ACTUAL:
      if (millis() - previousMillis >= interval) {
        previousMillis = millis();
        Serial.println("ACTL");
        Serial.println(iVal[2]);
        Serial.println(iVal[2]);
      }
      break;
    case MENU_TT1:
      if (millis() - previousMillis >= interval) {
        previousMillis = millis();
        Serial.println("TT 1");
        Serial.println(iVal[3]);
        Serial.println(iVal[3]);
      }
      break;
    case MENU_TT2:
      if (millis() - previousMillis >= interval) {
        previousMillis = millis();
        Serial.println("TT 2");
        Serial.println(iVal[4]);
        Serial.println(iVal[4]);
      }
      break;
    case MENU_TT3:
      if (millis() - previousMillis >= interval) {
        previousMillis = millis();
        Serial.println("TT 3");
        Serial.println(iVal[5]);
        Serial.println(iVal[5]);
      }
      break;
    case MENU_TT4:
      if (millis() - previousMillis >= interval) {
        previousMillis = millis();
        Serial.println("TT 4");
        Serial.println(iVal[6]);
        Serial.println(iVal[6]);
      }
      break;
    default:
      break;
  }
  Serial.println();
  Serial.println("************************************************");
  Serial.println();
}

uint8_t memWriteInt(uint8_t addr, uint32_t int_value)
{
  uint8_t ret = 0;
  if (!eeprom.eeprom_write(addr, int_value)) {
    ret = 1;
    Serial.println("Failed to store INT");
  } else {
    ret = 0;
    Serial.println("INT correctly stored");
  }
  return ret;
}

uint32_t memReadInt(uint8_t addr)
{
  uint32_t val_temp = 0;
  eeprom.eeprom_read(addr, &val_temp);
  Serial.println(val_temp);
  return val_temp;
}
/***************************************************************************************/

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  linkSerial.begin(9600);
  remote_cmd = RuN;
  remote_menu = MENU_HOME;
  pinMode(pin_TT1, INPUT_PULLUP);
  pinMode(pin_TT2, INPUT_PULLUP);
  pinMode(pin_TT3, INPUT_PULLUP);
  pinMode(pin_TT4, INPUT_PULLUP);
  pinMode(pin_counter, INPUT_PULLUP);
  Serial.println("Nungguin TT");

  //read eeprom
  Wire.begin();
//  memWriteInt(0, 0);
//  memWriteInt(1, 0);
//  memWriteInt(2, 0);
//  memWriteInt(3, 0);
//  memWriteInt(4, 0);
//  memWriteInt(5, 0);
//  memWriteInt(6, 0);

  iVal[0] = memReadInt(0);
  iVal[1] = memReadInt(1);
  iVal[2] = memReadInt(2);
  iVal[3] = memReadInt(3);
  iVal[4] = memReadInt(4);
  iVal[5] = memReadInt(5);
  iVal[6] = memReadInt(6);


  while (1)
  {

    if (!button_TT1) {
      Serial.println("TT1");
      // TT_Messg("1");
      TT_select = 3;
      temp_TT_select = 1;
      delay(2000);
      break;
    } else if (!button_TT2) {
      Serial.println("TT2");
      //TT_Messg("2");
      TT_select = 4;
      temp_TT_select = 2;
      delay(2000);
      break;
    } else if (!button_TT3) {
      Serial.println("TT3");
      //TT_Messg("3");
      TT_select = 5;
      temp_TT_select = 3;
      delay(2000);
      break;
    } else if (!button_TT4) {
      Serial.println("TT4");
      //TT_Messg("4");
      TT_select = 6;
      temp_TT_select = 4;
      delay(2000);
      break;
    }
  }



}

/***************************************************************************************/

void loop() {

  for (int i = 0; i <= 6; i++) {
    iVal_temp[i] = iVal[i];
  }

  if (remote_cmd == RuN || remote_cmd == ContinuE) {
    board_counting();
  }
  else if (remote_cmd == HolD) {
    board_hold();
  }
  else if (remote_cmd == SenD && remote_menu >= MENU_TARGET && remote_menu <= MENU_TT4) {
    board_changeval(remote_menu);
  }
  else if (remote_cmd == SenD && remote_menu == MENU_HOME) {
    board_hold();
  }

  // Check if the other Arduino is transmitting
  if (linkSerial.available())
  {
    // Allocate the JSON document
    // This one must be bigger than for the sender because it must store the strings
    StaticJsonDocument<128> doc;

    // Read the JSON document from the "link" serial port
    DeserializationError err = deserializeJson(doc, linkSerial);

    if (err == DeserializationError::Ok)
    {
      iVal[0] = doc["etgt"];
      iVal[1] = doc["ctgt"];
      iVal[2] = doc["actl"];
      iVal[3] = doc["tt1"];
      iVal[4] = doc["tt2"];
      iVal[5] = doc["tt3"];
      iVal[6] = doc["tt4"];
      remote_cmd = doc["cmd"];
      remote_menu = doc["menu"];

      char buf[16];
      sprintf(buf, "etgt: %d", iVal[0]);
      Serial.println(buf);
      sprintf(buf, "ctgt: %d", iVal[1]);
      Serial.println(buf);
      sprintf(buf, "actl: %d", iVal[2]);
      Serial.println(buf);
      sprintf(buf, "tt1: %d", iVal[3]);
      Serial.println(buf);
      sprintf(buf, "tt2: %d", iVal[4]);
      Serial.println(buf);
      sprintf(buf, "tt3: %d", iVal[5]);
      Serial.println(buf);
      sprintf(buf, "tt4: %d", iVal[6]);
      Serial.println(buf);
      sprintf(buf, "cmd: %d", remote_cmd);
      Serial.println(buf);
      sprintf(buf, "menu: %d", remote_menu);
      Serial.println(buf);
      Serial.println();
      Serial.println("************************************************");
      Serial.println();
    }
    else
    {
      // Print error to the "debug" serial port
      Serial.print("deserializeJson() returned ");
      Serial.println(err.c_str());

      // Flush all bytes in the "link" serial port buffer
      while (linkSerial.available() > 0)
        linkSerial.read();
    }
  }

//  for (int i = 0; i <= 6; i++) {
//    if (iVal_temp[i] != iVal[i]) {
//      iVal_temp[i] = iVal[i];
//      memWriteInt(i, iVal[i]);
//    }
//  }
  
}
