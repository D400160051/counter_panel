//===================================================================================================================================
#include "DMD3.h"
//#include "Droid_Sans_16.h"
#include "DejaVuSansBold9.h"
#include "angka6x13.h"

DMD3 display (1,3);
char dmdBuff1[10];
unsigned int cerah = 100;

//===================================================================================================================================
void Tampil_Hold()
{
    display.setFont(DejaVuSansBold9);
    display.drawText(5, 20, "Hold");
    
    display.drawText(5, 0, "____");
    display.drawText(5, 32, "____");
}
void Tampil_angka_T(int angka)
{
    sprintf(dmdBuff1, "%.4d", angka);
    display.setFont(angka6x13);
    display.drawText(3, 17, dmdBuff1);
}
void Tampil_angka_A(int angka)
{
    sprintf(dmdBuff1, "%.4d", angka);
    display.setFont(angka6x13);
    display.drawText(3, 1, dmdBuff1);
}
void Tampil_angka_B(int angka)
{
    sprintf(dmdBuff1, "%.4d", angka);
    display.setFont(angka6x13);
    display.drawText(3, 33, dmdBuff1);
}
void Angka_0()
{
     Tampil_angka_T(0000);
     Tampil_angka_A(0000);
     Tampil_angka_B(0000);
}
void Tampil_text_A(char text[10])
{
   
    display.setFont(DejaVuSansBold9);
    display.drawText(3, 3, text);
}
void Tampil_text_T(char text[10])
{
  
}
void Tampil_text_B(char text[10])
{
  
}
void TT_Messg(char tt[2])
{
    display.clear();
    display.setFont(DejaVuSansBold9);
    display.drawText(7, 3, "T T");
    display.setFont(angka6x13);
    display.drawText(13, 17, tt);
}
void etgt()
{
    Tampil_text_A("ETGT");
  
}
void ctgt()
{
    Tampil_text_A("CTGT");
}
void actl()
{
    Tampil_text_A("ACTL");
}
void TT_1()
{
   Tampil_text_A("TT 1");
}
void TT_2()
{
   Tampil_text_A("TT 2");
}
void TT_3()
{
   Tampil_text_A("TT 3");
}
void TT_4()
{
   Tampil_text_A("TT 4");
}
