/*
 * Pin use 
 * A0 => TT1                                  D0 => Rx                    D6  => A(p10)       D12 => 
 * A1 => TT2                                  D1 => Tx                    D7  => B(p10)       D13 => clk(p10)
 * A2 => TT3                                  D2 => tombol Counter        D8  => sclk(p10)
 * A3 => TT4                                  D3 =>                       D9  => Oe pwm(p10)
 * A4 => SDA                                  D4 =>                       D10 =>
 * A5 => SCL                                  D5 =>                       D11 => data(p10)
 * 
*/
#include "DMD3.h"
#include "Droid_Sans_16.h"
#include "DejaVuSansBold9.h"
#include "angka6x13.h"
#include <EEPROM.h>



#define pin_counter1 2
#define pin_TT1 A0
#define pin_TT2 A1 
#define pin_TT3 A2
#define pin_TT4 A3
#define AO 9

#define button_TT1 digitalRead(pin_TT1)
#define button_TT2 digitalRead(pin_TT2)
#define button_TT3 digitalRead(pin_TT3)
#define button_TT4 digitalRead(pin_TT4)

#define button_counter1 digitalRead(pin_counter1)


#define button_notpressed  button_TT1 && button_TT2 && button_TT3 && button_TT4 && button_counter1
#define button_pressed !(button_notpressed)

int addr = 100;


DMD3 display (1,3);
char dmdBuff1[10];
unsigned int cerah = 100;

unsigned long lastTime = 0;
unsigned long timerDelay = 1000;
unsigned int temp_tt = 0;
int lock = 0;

unsigned int counter = 3000;
unsigned int actual  = 0;
unsigned int diff    = 0;
unsigned int TT[5];
//=============

//=============
byte TT_select;

void scan()
{
    display.refresh();
}


void setup() {
  
  pinMode(pin_TT1, INPUT_PULLUP);
  pinMode(pin_TT2, INPUT_PULLUP);
  pinMode(pin_TT3, INPUT_PULLUP);
  pinMode(pin_TT4, INPUT_PULLUP);
  pinMode(pin_counter1, INPUT_PULLUP);
  
TT[1] = 10;
TT[2] = 10;
TT[3] = 10;
TT[4] = 10;

  
    Timer1.initialize(1000);
    Timer1.attachInterrupt(scan);
    Timer1.pwm(AO,cerah);
    Serial.begin(9600);
   

   
Tampil_Hold();
Serial.println("HOLD kunci");

while(1)
{   // parsing passoword mode kunci 

     if (!button_TT1) {
      Serial.println("TT1");
        TT_Messg("1");
        TT_select = 1;
      delay(2000);
      break;
    }else
     if (!button_TT2) {
      Serial.println("TT2");
        TT_Messg("2");
        TT_select = 2;
      delay(2000);
      break;
    }else
     if (!button_TT3) {
      Serial.println("TT3");
        TT_Messg("3");
        TT_select = 3;
      delay(2000);
      break;
    }else
     if (!button_TT4) {
      Serial.println("TT4");
        TT_Messg("4");
        TT_select = 4;
      delay(2000);
      break;
    }
}
    display.clear();
    Tampil_Hold();

    
}

void loop() 
{
  //Shome();
   RUN();
  //Tampil_text_A("ACTL");
  //TT_1();

 
}

//============================================================================================================================================================
//                                                                                 FUNSI PROSES 
//============================================================================================================================================================


void Shome()
{
     Tampil_angka_A(counter);
     Tampil_angka_T(actual);
     Tampil_angka_B(diff);
     Serial.println("HOME");
      //parsing mode run/config
     
}


void RUN()
{Serial.println("RUNING COUNTER");

  while(1){
     
    if (!button_counter1 && lock < 0 ) {
      lock = 1;
      Serial.println("reset TT");
      temp_tt = 0;
      if (diff > 0)
      {
        diff = diff - 1;
        actual++;
      }
      else
      {
        actual++;         
      }
      delay(100);
    }
  P10();
  //parsing stop conter
delay (10);
}
}

void menu_set()
{ 
    Serial.println("Mode Setting");
    while (1) {
             
           // parsing setting 
    
       delay (10);
}}

void P10()
{
  if ((millis() - lastTime) > timerDelay) {
     Serial.print("runing = "); 
     Serial.print(temp_tt);
     Serial.print(" , counter = "); 
     Serial.print(counter);
     Serial.print(" , actual = "); 
     Serial.print(actual);
     Serial.print(" , diff = - "); 
     Serial.println(diff);
     
     Tampil_angka_A(counter);
     Tampil_angka_T(actual);
     Tampil_angka_B(diff);
     
temp_tt = temp_tt+1;
lock=lock-1;
if (temp_tt >= TT[TT_select]){temp_tt=0;Serial.println("minus 1");diff++;lock =1;}
    lastTime = millis();
  }

}



//============================================================================================================================================================
//                                                                                   FUNSI TAMPILAN 
//============================================================================================================================================================

void etgt()
{
    Tampil_text_A("ETGT");
  
}
void ctgt()
{
    Tampil_text_A("CTGT");
}
void actl()
{
    Tampil_text_A("ACTL");
}
void TT_1()
{
   Tampil_text_A("TT 1");
}
void TT_2()
{
   Tampil_text_A("TT 2");
}
void TT_3()
{
   Tampil_text_A("TT 3");
}
void TT_4()
{
   Tampil_text_A("TT 4");
}

void Tampil_Hold()
{
    display.setFont(DejaVuSansBold9);
    display.drawText(5, 20, "Hold");
    
    display.drawText(5, 0, "____");
    display.drawText(5, 32, "____");
}
void Tampil_angka_T(int angka)
{
    sprintf(dmdBuff1, "%.4d", angka);
    display.setFont(angka6x13);
    display.drawText(3, 17, dmdBuff1);
}
void Tampil_angka_A(int angka)
{
    sprintf(dmdBuff1, "%.4d", angka);
    display.setFont(angka6x13);
    display.drawText(3, 1, dmdBuff1);
}
void Tampil_angka_B(int angka)
{
    sprintf(dmdBuff1, "%.4d", angka);
    display.setFont(angka6x13);
    display.drawText(3, 33, dmdBuff1);
}
void Angka_0()
{
     Tampil_angka_T(0000);
     Tampil_angka_A(0000);
     Tampil_angka_B(0000);
}
void Tampil_text_A(char text[10])
{
   
    display.setFont(DejaVuSansBold9);
    display.drawText(3, 3, text);
}
void Tampil_text_T(char text[10])
{
  
}
void Tampil_text_B(char text[10])
{
  
}
void TT_Messg(char tt[2])
{
    display.clear();
    display.setFont(DejaVuSansBold9);
    display.drawText(7, 3, "T T");
    display.setFont(angka6x13);
    display.drawText(13, 17, tt);
}
